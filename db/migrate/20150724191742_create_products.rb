class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :ProductName
      t.text :ProductExtra
      t.string :ProductStatus
      t.boolean :ProductVisibality
      t.float :ProductPrice
      t.string :ProductIdentifire

      t.timestamps null: false
    end
  end
end
