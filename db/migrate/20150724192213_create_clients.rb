class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :ClientName
      t.string :ClientPhone
      t.string :ClientAddress
      t.string :OrderItem
      t.boolean :isDeleted
      t.boolean :isActive
      t.boolean :isValid
      t.boolean :isRecived

      t.timestamps null: false
    end
  end
end
