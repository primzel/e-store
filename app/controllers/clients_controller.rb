class ClientsController < ApplicationController
  def index
  end

  def show
  end

  def edit
  end

  def update
  end

  def delete
  end

  def destroy
  end

  def new
  end

  def create
    @client=Client.new(access_attrs)
    if @client.save
      redirect_to :controller=>"products", :action=>"show", :id=>params[:id], :message=>"success"
    else
      redirect_to :controller=>"products", :action=>"show", :id=>params[:id], :message=>"error"
    end
  end


def access_attrs
  params.require(:client).permit(:ClientName,:email,:ClientPhone,:ClientAddress,:OrderItem)
end

end
