class ProductsController < ApplicationController

  layout "products" 


  def index

    @bags=Product.all
  end

  def show

    @bag=Product.find(params[:id])

    render :layout =>"application"
  end

  def new
  end

  def create
  end

  def edit
  end

  def update
  end

  def delete
  end

  def destroy
  end
end
